<?php

require dirname(__DIR__) . '/vendor/autoload.php';

error_reporting(E_ALL);
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');

session_start();

$router = new Core\Router();

$router->add('community/{category:[^/]+}/{topic:[^/]+}/{thread:[^/]+}', ['controller' => 'Community', 'action' => 'thread']);
$router->add('community/{topic:[^/]+}/newThread', ['controller' => 'Community', 'action' => 'newThread']);
$router->add('community/{category:[^/]+}/{topic:[^/]+}', ['controller' => 'Community', 'action' => 'topic']);
$router->add('community', ['controller' => 'Community', 'action' => 'index']);

$router->add('', ['controller' => 'Home', 'action' => 'index']);

$router->add('login', ['controller' => 'Login', 'action' => 'new']);
$router->add('logout', ['controller' => 'Login', 'action' => 'destroy']);
$router->add('password/reset/{token:[\da-f]+}', ['controller' => 'Password', 'action' => 'reset']);
$router->add('signup/activate/{token:[\da-f]+}', ['controller' => 'Signup', 'action' => 'activate']);

$router->add('profile', ['controller' => 'Profile', 'action' => 'show']);
$router->add('admin/{controller}/{action}/{title:[^/]+}', ['namespace' => 'Admin']);
$router->add('admin/{controller}/{action}', ['namespace' => 'Admin']);

$router->add('article/{title:[^/]+}', ['controller' => 'Article', 'action' => 'show']);

$router->add('{controller}/{action}');
$router->add('{controller}/{id:\d+}/{action}');
$router->add('{controller}/{action}/{title:[^/]+}');

$router->dispatch($_SERVER['QUERY_STRING']);
