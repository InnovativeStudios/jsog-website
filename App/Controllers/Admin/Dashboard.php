<?php

namespace App\Controllers\Admin;

use \Core\View;
use \App\Models\Admin;


class Dashboard extends \Core\Controller {
    protected function before() {
        $this->requireLogin();
    }

    public function indexAction() {
        $article = Admin::getAllArticles();
        $category = Admin::getAllCategories();
        $user = Admin::getAllUsers();
        $forumCategory = Admin::getAllForumCategories();
        $forumTopic = Admin::getAllForumTopics();
        $userGroup = Admin::getAllUserGroups();
        View::renderTemplate('Admin/index.html', [
            'articles' => $article,
            'categories' => $category,
            'users' => $user,
            'forumTopics' => $forumTopic,
            'forumCategories' => $forumCategory,
            'userGroups' => $userGroup
        ]);
    }
}