<?php

namespace App\Controllers\Admin;

use \Core\View;
use \App\Models\Admin;
use \App\Models\Forum;
use \App\Flash;


class Community extends \Core\Controller {
    protected function before() {
        $this->requireLogin();
    }

    public function categoryAction() {
        $forum_categories = Admin::getAllForumCategories();
        View::renderTemplate('Admin/forum-category.html', [
            'forum_categories' => $forum_categories
        ]);
    }

    public function editCategoryAction() {
        View::renderTemplate('Admin/editForumCategory.html', [
            'forum_category' => Forum::findByCategoryTitle($this->route_params['title'])
        ]);
    }

    public function deleteCategoryAction() {
        $forum_category = Forum::findByCategoryTitle($this->route_params['title']);
        if($forum_category->deleteForumCategory()) {
            Flash::addMessage('Forum Category Successfully Deleted');
            $this->redirect('/admin/community/category');
        } else {
            View::renderTemplate('Admin/forum-category.html', [
                'forum_category' => $forum_category
            ]);
        }
    }

    public function updateCategoryAction() {
        $forum_category = Forum::findByCategoryTitle($this->route_params['title']);
        if($forum_category->updateForumCategory($_POST)) {
            Flash::addMessage('Forum Category Successfully Updated');
            $this->redirect('/admin/community/category');
        } else {
            View::renderTemplate('Admin/forum-category.html', [
                'forum_category' => $forum_category
            ]);
        }
    }

    public function createCategoryAction() {
        $forum_category = new Forum($_POST);

        if ($forum_category->saveForumCategory()) {
            Flash::addMessage('Forum Category Successfully Created');
            $this->redirect('/admin/community/category');
        } else {
            View::renderTemplate('Admin/forum-category.html', [
                'forum_category' => $forum_category
            ]);
        }
    }

    public function topicAction() {
        $forum_topics = Admin::getAllForumTopics();
        View::renderTemplate('Admin/forum-topic.html', [
            'forum_topics' => $forum_topics,
            'forum_category' => Forum::getForumCategories()
        ]);
    }

    public function editTopicAction() {
        View::renderTemplate('Admin/editForumTopic.html', [
            'forum_topic' => Forum::findByTopicTitle($this->route_params['title']),
            'forum_category' => Forum::getForumCategories()
        ]);
    }

    public function deleteTopicAction() {
        $forum_topic = Forum::findByTopicTitle($this->route_params['title']);
        if($forum_topic->deleteForumTopic()) {
            Flash::addMessage('Forum Topic Successfully Deleted');
            $this->redirect('/admin/community/topic');
        } else {
            View::renderTemplate('Admin/forum-topic.html', [
                'forum_topic' => $forum_topic
            ]);
        }
    }

    public function updateTopicAction() {
        $forum_topic = Forum::findByTopicTitle($this->route_params['title']);
        if($forum_topic->updateForumTopic($_POST)) {
            Flash::addMessage('Forum Topic Successfully Updated');
            $this->redirect('/admin/community/topic');
        } else {
            View::renderTemplate('Admin/forum-topic.html', [
                'forum_topic' => $forum_topic,
                'forum_category' => Forum::getForumCategories()
            ]);
        }
    }

    public function createTopicAction() {
        $forum_topic = new Forum($_POST);

        if ($forum_topic->saveForumTopic()) {
            Flash::addMessage('Forum Topic Successfully Created');
            $this->redirect('/admin/community/topic');
        } else {
            View::renderTemplate('Admin/forum-topic.html', [
                'forum_topic' => $forum_topic,
                'forum_category' => Forum::getForumCategories()
            ]);
        }
    }

    public function postAction() {
        $forum_posts = Admin::getAllForumPosts();
        View::renderTemplate('Admin/forum-post.html', [
            'forum_posts' => $forum_posts
        ]);
    }
}