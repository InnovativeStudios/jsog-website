<?php

namespace App\Controllers\Admin;

use \Core\View;
use \App\Models\Admin;
use \App\Models\BlogCategory;
use \App\Flash;

class Category extends \Core\Controller {
    protected function before() {
        $this->requireLogin();
    }

    public function indexAction() {
        $category = Admin::getAllCategories();
        View::renderTemplate('Admin/category.html', [
            'categories' => $category
        ]);
    }

    public function editAction() {
        View::renderTemplate('Admin/editCategory.html', [
            'category' => BlogCategory::findByCategoryTitle($this->route_params['title'])
        ]);
    }

    public function deleteAction() {
        $category = BlogCategory::findByCategoryTitle($this->route_params['title']);
        if($category->deleteCategory()) {
            Flash::addMessage('Category Successfully Deleted');
            $this->redirect('/admin/category/index');
        } else {
            View::renderTemplate('Admin/category.html', [
                'categories' => $category
            ]);
        }
    }

    public function updateAction() {
        $category = BlogCategory::findByCategoryTitle($this->route_params['title']);
        if($category->updateCategory($_POST)) {
            Flash::addMessage('Category Successfully Updated');
            $this->redirect('/admin/category/index');
        } else {
            View::renderTemplate('Admin/category.html', [
                'categories' => $category
            ]);
        }
    }

    public function createAction() {
        $category = new BlogCategory($_POST);

        if ($category->save()) {
            Flash::addMessage('Category Successfully Created');
            $this->redirect('/admin/category/index');
        } else {
            View::renderTemplate('Admin/category.html', [
                'categories' => $category
            ]);
        }
    }
}