<?php

namespace App\Controllers\Admin;

use \Core\View;
use \App\Models\Admin;
use \App\Models\UserGroup;


class User extends \Core\Controller {
    protected function before() {
        $this->requireLogin();
    }

    public function indexAction() {
        $user = Admin::getAllUsers();
        View::renderTemplate('Admin/user.html', [
            'users' => $user
        ]);
    }

    public function groupAction() {
        $user_group = Admin::getAllUserGroups();
        View::renderTemplate('Admin/user-group.html', [
            'user_groups' => $user_group
        ]);
    }
}