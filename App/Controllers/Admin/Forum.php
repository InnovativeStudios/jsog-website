<?php

namespace App\Controllers\Admin;

use \Core\View;
use \App\Models\Forum;
use \App\Models\Admin;


class Forum extends \Core\Controller {
    protected function before() {
        $this->requireLogin();
    }

    public function categoryAction() {
        $forum_categories = Admin::getAllForumCategories();
        View::renderTemplate('Admin/forum-category.html', [
            'forum_categories' => $forum_categories
        ]);
    }

    public function categoryEditAction() {
        View::renderTemplate('Admin/editforum_category.html', [
            'forum_category' => Forum::findByCategoryTitle($this->route_params['title'])
        ]);
    }

    public function categoryDeleteAction() {
        $forum_category = Forum::findByCategoryTitle($this->route_params['title']);
        if($forum_category->deleteCategory()) {
            Flash::addMessage('Forum Category Successfully Deleted');
            $this->redirect('/admin/forum/category');
        } else {
            View::renderTemplate('Admin/forum-category.html', [
                'forum_category' => $forum_category
            ]);
        }
    }

    public function categoryUpdateAction() {
        $forum_category = Forum::findByCategoryTitle($this->route_params['title']);
        if($forum_category->updateForumCategory($_POST)) {
            Flash::addMessage('Forum Category Successfully Updated');
            $this->redirect('/admin/forum/category');
        } else {
            View::renderTemplate('Admin/forum-category.html', [
                'forum_category' => $forum_category
            ]);
        }
    }

    public function categoryCreateAction() {
        $forum_category = new Forum($_POST);

        if ($forum_category->save()) {
            Flash::addMessage('Forum Category Successfully Created');
            $this->redirect('/admin/forum/category');
        } else {
            View::renderTemplate('Admin/forum-category.html', [
                'forum_category' => $forum_category
            ]);
        }
    }

    public function topicAction() {
        $forum_topics = Admin::getAllForumTopics();
        View::renderTemplate('Admin/forum-topic.html', [
            'forum_topics' => $forum_topics
        ]);
    }

    public function postAction() {
        $forum_posts = Admin::getAllForumPosts();
        View::renderTemplate('Admin/forum-post.html', [
            'forum_posts' => $forum_posts
        ]);
    }
}