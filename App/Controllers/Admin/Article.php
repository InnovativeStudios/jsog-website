<?php

namespace App\Controllers\Admin;

use \Core\View;
use \App\Models\Blog;
use \App\Flash;

class Article extends \Core\Controller {
    protected function before() {
        $this->requireLogin();
    }

    public function newAction() {
        View::renderTemplate('Admin/newArticle.html', [
            'category' => Blog::getAllCategories()
        ]);
    }

    public function indexAction() {
        $blog = Blog::getAllArticlesAdmin();
        View::renderTemplate('Admin/article.html', [
            'blog' => $blog
        ]);
    }

    public function showAction() {
        View::renderTemplate('Article/show.html', [
            'blog' => Blog::findByArticleTitle($this->route_params['title'])
        ]);
    }

    public function editAction() {
        View::renderTemplate('Admin/editArticle.html', [
            'blog' => Blog::findByArticleTitle($this->route_params['title']),
            'category' => Blog::getAllCategories()
        ]);
    }

    public function updateAction() {
        $blog = Blog::findByArticleTitle($this->route_params['title']);
        if($blog->updateArticle($_POST, $_FILES)) {
            Flash::addMessage('Article Successfully Updated');
            $this->redirect('/admin/article/index');
        } else {
            View::renderTemplate('Admin/editArticle.html', [
                'blog' => $blog
            ]);
        }
    }

    public function deleteAction() {
        $blog = Blog::findByArticleTitle($this->route_params['title']);
        if($blog->deleteArticle()) {
            Flash::addMessage('Article Successfully Deleted');
            $this->redirect('/admin/article/index');
        } else {
            View::renderTemplate('Admin/article.html', [
                'blog' => $blog
            ]);
        }
    }

    public function createAction() {
        $blog = new Blog($_POST, $_FILES);

        if ($blog->save()) {
            Flash::addMessage('Article Successfully Created');
            $this->redirect('/admin/article/index');
        } else {
            View::renderTemplate('Admin/newArticle.html', [
                'article' => $blog
            ]);
        }
    }

    public function validateTitleAction() {
        $is_valid = ! Blog::articleExists($_GET['article_title'], $_GET['ignore_id'] ?? null);

        header('Content-Type: application/json');
        echo json_encode($is_valid);
    }
}