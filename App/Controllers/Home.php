<?php

namespace App\Controllers;

use \Core\View;
use \App\Auth;
use \App\Models\Blog;
use \App\Models\User;

class Home extends \Core\Controller {
    public function indexAction() {
        $article = Blog::getAllArticles();
        $featureArticle = Blog::getAllFeatureArticles();
        $category = Blog::getAllCategories();
        $user_online = Blog::getAllRememberedLogins();
        View::renderTemplate('Home/index.html', [
            'articles' => $article,
            'featureArticles' => $featureArticle,
            'categories' => $category,
            'users_online' => $user_online
        ]);
    }
}