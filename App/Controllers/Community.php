<?php

namespace App\Controllers;

use \Core\View;
use \App\Flash;
use \App\Models\Forum;

class Community extends \Core\Controller {
    public function indexAction() {
        $community = Forum::getAllForumCategories();
        $topicThread = Forum::getTopicThreads();
        $onlineUser = Forum::getAllOnlineUsers();
        View::renderTemplate('Community/index.html', [
            'community' => $community,
            'threads' => $topicThread,
            'onlineUsers' => $onlineUser
        ]);
    }

    public function topicAction() {
        $topicCategory = Forum::findByCategoryTitle($this->route_params['category']);
        $forumTopic = Forum::findByTopicTitle($this->route_params['topic']);
        $topic = Forum::findByTopicTitle($this->route_params['topic']);
        $topicThread = Forum::getTopicThreads();
        View::renderTemplate('Community/topic.html', [
            'topic' => $topic->getThreads(),
            'topicCategory' => $topicCategory,
            'forumTopic' => $forumTopic,
            'threads' => $topicThread
        ]);
    }

    public function threadAction() {
        $threadTopicCategory = Forum::findByCategoryTitle($this->route_params['category']);
        $threadTopic = Forum::findByTopicTitle($this->route_params['topic']);
        $threadPost = Forum::findByThreadTitle($this->route_params['thread']);
        $thread = Forum::findByThreadTitle($this->route_params['thread']);
        View::renderTemplate('Community/thread.html', [
            'thread' => $thread,
            'threadPost' => $threadPost->getPosts(),
            'threadTopic' => $threadTopic,
            'threadTopicCategory' => $threadTopicCategory
        ]);
    }

    public function newThreadAction() {
        $title = $this->route_params['topic'];
        $threadTopic = Forum::findByTopicTitle($this->route_params['topic']);
        View::renderTemplate('Community/newThread.html', [
            'threadTopic' => $threadTopic
        ]);
    }

    public function replyToThreadAction() {
        $threadPost = new Forum($_POST);
        $id = $threadPost->saveForumThreadPost();

        if ($id !== false) {
            Flash::addMessage('Post Successfully Created');
            $this->redirect('/Community/index');
        } else {
            Flash::addMessage('Post Was Not Successfully Created');
            $this->redirect('/Community/index');
        }
    }

    public function createThreadAction() {
        $forum = new Forum($_POST);

        if ($forum->saveForumThread()) {
            Flash::addMessage('Thread Successfully Created');
            $this->redirect('/community/index');
        } else {
            View::renderTemplate('Community/newThread.html', [
                'forum' => $forum
            ]);
        }
    }
}