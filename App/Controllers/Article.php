<?php

namespace App\Controllers;

use \Core\View;
use \App\Auth;
use \App\Models\Blog;
use \App\Models\User;

class Article extends \Core\Controller {
    public function showAction() {
        View::renderTemplate('Article/show.html', [
            'blog' => Blog::findByArticleTitle($this->route_params['title'])
        ]);
    }
}