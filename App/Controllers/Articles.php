<?php

namespace App\Controllers;

use \Core\View;
use \App\Auth;
use \App\Models\Blog;

class Articles extends \Core\Controller {
    public function indexAction() {
        $article = Blog::getAllArticles();
        $category = Blog::getAllCategories();
        $user_online = Blog::getAllRememberedLogins();
        View::renderTemplate('Articles/index.html', [
            'articles' => $article,
            'categories' => $category,
            'users_online' => $user_online
        ]);
    }
}