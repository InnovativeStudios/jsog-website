<?php

namespace App\Controllers;

use \Core\View;
use \App\Auth;
use \App\Models\User;

class Media extends \Core\Controller {
    public function indexAction() {
        View::renderTemplate('Media/index.html', []);
    }
}
