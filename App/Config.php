<?php

namespace App;

class Config {
    const DB_HOST = 'localhost';
    const DB_NAME = 'jsog';
    const DB_USER = 'root';
    const DB_PASSWORD = '';

    const SHOW_ERRORS = false;

    const SECRET_KEY = 'f8i96LXqSphlQV6o8n5PyeGvjcbksL8c';

    const MAILGUN_API_KEY = 'key-0a65dab3b8983a08bd5e58bc7fcad4d3';
    const MAILGUN_DOMAIN = 'mg.innovativestudios.net';
}
