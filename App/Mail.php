<?php

namespace App;

use App\Config;
use Mailgun\Mailgun;

class Mail {
    public static function send($to, $subject, $text, $html) {
        $mg = new Mailgun(Config::MAILGUN_API_KEY);
        $domain = Config::MAILGUN_DOMAIN;

        $mg->sendMessage($domain, ['from'    => 'postmaster@mg.innovativestudios.net',
                                   'to'      => $to,
                                   'subject' => $subject,
                                   'text'    => $text,
                                   'html'    => $html
        ]);
    }
}
