<?php

namespace App\Models;

use PDO;
use \App\Token;
use \App\Mail;
use \Core\View;

class User extends \Core\Model {
    public $errors = [];

    public function __construct($data = []) {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        };
    }

    public function save() {
        $this->validate();

        if (empty($this->errors)) {
            $password_hash = password_hash($this->user_password, PASSWORD_DEFAULT);

            $token = new Token();
            $hashed_token = $token->getHash();
            $this->activation_token = $token->getValue();

            $rand = rand(1, 16);
            if($rand == 1)
                $this->user_image = "head_alizarin.png";
            else if($rand == 2)
                $this->user_image = "head_amethyst.png";
            else if($rand == 3)
                $this->user_image = "head_belize_hole.png";
            else if($rand == 4)
                $this->user_image = "head_carrot.png";
            else if($rand == 5)
                $this->user_image = "head_deep_blue.png";
            else if($rand == 6)
                $this->user_image = "head_emerald.png";
            else if($rand == 7)
                $this->user_image = "head_green_sea.png";
            else if($rand == 8)
                $this->user_image = "head_nephritis.png";
            else if($rand == 9)
                $this->user_image = "head_pete_river.png";
            else if($rand == 10)
                $this->user_image = "head_pomegranate.png";
            else if($rand == 11)
                $this->user_image = "head_pumpkin.png";
            else if($rand == 12)
                $this->user_image = "head_red.png";
            else if($rand == 13)
                $this->user_image = "head_sun_flower.png";
            else if($rand == 14)
                $this->user_image = "head_turqoise.png";
            else if($rand == 15)
                $this->user_image = "head_wet_asphalt.png";
            else if($rand == 16)
                $this->user_image = "head_wisteria.png";

            $sql = 'INSERT INTO users (username, user_email, randSalt, activation_hash, user_first_name, user_last_name, user_image, user_signup_date)
            VALUES (:username, :user_email, :password_hash, :activation_hash, :user_first_name, :user_last_name, :user_image, NOW())';

            $db = static::getDB();
            $stmt = $db->prepare($sql);

            $stmt->bindValue(':username', $this->username, PDO::PARAM_STR);
            $stmt->bindValue(':user_email', $this->user_email, PDO::PARAM_STR);
            $stmt->bindValue(':password_hash', $password_hash, PDO::PARAM_STR);
            $stmt->bindValue(':activation_hash', $hashed_token, PDO::PARAM_STR);
            $stmt->bindValue(':user_first_name', $this->user_first_name, PDO::PARAM_STR);
            $stmt->bindValue(':user_last_name', $this->user_last_name, PDO::PARAM_STR);
            $stmt->bindValue(':user_image', $this->user_image, PDO::PARAM_STR);

            return $stmt->execute();
        }
        return false;
    }

    public function validate() {
        if ($this->username == '') {
            $this->errors[] = 'Username is Required';
        }

        if ($this->user_first_name == '') {
            $this->errors[] = 'First Name is Required';
        }

        if ($this->user_last_name == '') {
            $this->errors[] = 'Last Name is Required';
        }

        if (filter_var($this->user_email, FILTER_VALIDATE_EMAIL) === false) {
            $this->errors[] = 'Invalid Email Address';
        }

        if (static::emailExists($this->user_email, $this->user_id ?? null)) {
            $this->errors[] = 'Email Address has Already been Taken';
        }

        if(isset($this->user_password)) {
            if (strlen($this->user_password) < 6) {
                $this->errors[] = 'Please Enter at Least 6 Characters for the Password';
            }

            if (preg_match('/.*[a-z]+.*/i', $this->user_password) == 0) {
                $this->errors[] = 'Password Needs at Least One Letter';
            }

            if (preg_match('/.*\d+.*/i', $this->user_password) == 0) {
                $this->errors[] = 'Password Needs at Least One Number';
            }
        }
    }

    public static function emailExists($user_email, $ignore_id = null) {
        $user = static::findByEmail($user_email);

        if ($user) {
            if ($user->user_id != $ignore_id) {
                return true;
            }
        }
        return false;
    }

    public static function findByEmail($user_email) {
        $sql = 'SELECT * FROM users WHERE user_email = :user_email';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':user_email', $user_email, PDO::PARAM_STR);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }

    public static function findByUserName($username) {
        $sql = 'SELECT * FROM users WHERE username = :username';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':username', $username, PDO::PARAM_STR);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }

    public static function authenticate($username, $user_password) {
        $user = static::findByUserName($username);

        if ($user && $user->is_active) {
            if (password_verify($user_password, $user->randSalt)) {
                return $user;
            }
        }
        return false;
    }

    public static function findByID($user_id) {
        $sql = 'SELECT * FROM users WHERE user_id = :user_id';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':user_id', $user_id, PDO::PARAM_INT);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }

    public function rememberLogin() {
        $token = new Token();
        $hashed_token = $token->getHash();
        $this->remember_token = $token->getValue();

        $this->expiry_timestamp = time() + 60 * 60 * 24 * 30;  // 30 days from now

        $sql = 'INSERT INTO remembered_logins (token_hash, user_id, expires_at) VALUES(:token_hash, :user_id, :expires_at)';

        $db = static::getDB();
        $stmt = $db->prepare($sql);

        $stmt->bindValue(':token_hash', $hashed_token, PDO::PARAM_STR);
        $stmt->bindValue(':user_id', $this->user_id, PDO::PARAM_INT);
        $stmt->bindValue(':expires_at', date('Y-m-d H:i:s', $this->expiry_timestamp), PDO::PARAM_STR);

        return $stmt->execute();
    }

    public static function sendPasswordReset($user_email) {
        $user = static::findByEmail($user_email);

        if ($user) {
            if ($user->startPasswordReset()) {
                $user->sendPasswordResetEmail();
            }
        }
    }

    protected function startPasswordReset() {
        $token = new Token();
        $hashed_token = $token->getHash();
        $this->randSalt_reset_token = $token->getValue();

        $expiry_timestamp = time() + 60 * 60 * 2;  // 2 hours from now

        $sql = 'UPDATE users SET randSalt_reset_hash = :token_hash, randSalt_reset_expires_at = :expires_at WHERE user_id = :user_id';

        $db = static::getDB();
        $stmt = $db->prepare($sql);

        $stmt->bindValue(':token_hash', $hashed_token, PDO::PARAM_STR);
        $stmt->bindValue(':expires_at', date('Y-m-d H:i:s', $expiry_timestamp), PDO::PARAM_STR);
        $stmt->bindValue(':user_id', $this->user_id, PDO::PARAM_INT);

        return $stmt->execute();
    }

    protected function sendPasswordResetEmail() {
        $url = 'http://' . $_SERVER['HTTP_HOST'] . '/password/reset/' . $this->randSalt_reset_token;

        $text = View::getTemplate('Password/reset_email.txt', ['url' => $url]);
        $html = View::getTemplate('Password/reset_email.html', ['url' => $url]);

        Mail::send($this->user_email, 'Password reset', $text, $html);
    }

    public static function findByPasswordReset($token) {
        $token = new Token($token);
        $hashed_token = $token->getHash();

        $sql = 'SELECT * FROM users WHERE randSalt_reset_hash = :token_hash';

        $db = static::getDB();
        $stmt = $db->prepare($sql);

        $stmt->bindValue(':token_hash', $hashed_token, PDO::PARAM_STR);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        $user = $stmt->fetch();

        if ($user) {
            if (strtotime($user->randSalt_reset_expires_at) > time()) {
                return $user;
            }
        }
    }

    public function resetPassword($user_password) {
        $this->user_password = $user_password;

        $this->validate();

        if (empty($this->errors)) {
            $password_hash = password_hash($this->user_password, PASSWORD_DEFAULT);

            $sql = 'UPDATE users SET randSalt = :password_hash, randSalt_reset_hash = NULL, randSalt_reset_expires_at = NULL WHERE user_id = :user_id';

            $db = static::getDB();
            $stmt = $db->prepare($sql);

            $stmt->bindValue(':user_id', $this->user_id, PDO::PARAM_INT);
            $stmt->bindValue(':password_hash', $password_hash, PDO::PARAM_STR);

            return $stmt->execute();
        }
        return false;
    }

    public function sendActivationEmail() {
        $url = 'http://' . $_SERVER['HTTP_HOST'] . '/signup/activate/' . $this->activation_token;

        $text = View::getTemplate('Signup/activation_email.txt', ['url' => $url]);
        $html = View::getTemplate('Signup/activation_email.html', ['url' => $url]);

        Mail::send($this->user_email, 'Account activation', $text, $html);
    }

    public static function activate($value) {
        $token = new Token($value);
        $hashed_token = $token->getHash();

        $sql = 'UPDATE users SET is_active = 1, activation_hash = null WHERE activation_hash = :hashed_token';

        $db = static::getDB();
        $stmt = $db->prepare($sql);

        $stmt->bindValue(':hashed_token', $hashed_token, PDO::PARAM_STR);

        $stmt->execute();
    }

    public function updateProfile($data) {
        $this->username = $data['username'];
        $this->user_email = $data['user_email'];

        if($data['user_password'] != '') {
            $this->user_password = $data['user_password'];
        }

        $this->validate();

        if(empty($this->errors)) {
            $sql = 'UPDATE users SET username = :username, user_email = :user_email';

            if(isset($this->user_password)) {
                $sql .= ', randSalt = :password_hash';
            }

            $sql .= "\nWHERE user_id = :user_id'";

            $db = static::getDB();
            $stmt = $db->prepare($sql);

            $stmt->bindValue(':username', $this->username, PDO::PARAM_STR);
            $stmt->bindValue(':user_email', $this->user_email, PDO::PARAM_STR);
            $stmt->bindValue(':user_id', $this->user_id, PDO::PARAM_INT);

            if(isset($this->user_password)) {
                $password_hash = password_hash($this->user_password, PASSWORD_DEFAULT);
                $stmt->bindValue(':password_hash', $password_hash, PDO::PARAM_STR);
            }
            return $stmt->execute();
        }
        return false;
    }

    public function hasRole($role) {
        return $this->user_role == $role;
    }
}