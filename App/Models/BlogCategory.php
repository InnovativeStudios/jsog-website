<?php
/**
 * Created by PhpStorm.
 * User: Jacob Schmidt
 * Date: 8/14/2017
 * Time: 10:30 PM
 */

namespace App\Models;

use PDO;


class BlogCategory extends \Core\Model
{
    public $errors = [];

    public function __construct($data = []) {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        };
    }

    public function validate() {
        if ($this->category_title == '') {
            $this->errors[] = 'Category Title is Required';
        }
    }

    public function save() {

        $this->validate();

        if (empty($this->errors)) {
            $sql = 'INSERT INTO categories (category_title)
            VALUES (:category_title)';

            $db = static::getDB();
            $stmt = $db->prepare($sql);

            $stmt->bindValue(':category_title', $this->category_title, PDO::PARAM_STR);

            return $stmt->execute();
        }
        return false;
    }

    public static function categoryExists($category_title, $ignore_id = null) {
        $category = static::findByCategoryTitle($category_title);

        if ($category) {
            if ($category->category_id != $ignore_id) {
                return true;
            }
        }
        return false;
    }

    public static function getCategoryTitle($category_title) {
        $sql = 'SELECT * FROM categories WHERE category_title = $category_title';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue('category_title', $category_title, PDO::PARAM_INT);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }

    public static function findByCategoryTitle($category_title) {
        $sql = 'SELECT * FROM categories WHERE category_title = :category_title';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':category_title', $category_title, PDO::PARAM_STR);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }

    public static function findByCategoryID($category_id) {
        $sql = 'SELECT * FROM categories WHERE category_id = :category_id';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':category_id', $category_id, PDO::PARAM_INT);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }

    public function deleteCategory() {

        $this->validate();               

        if(empty($this->errors)) {             

            $sql = 'DELETE FROM categories WHERE category_id = :category_id';
            
            $db = static::getDB();
            $stmt = $db->prepare($sql);
            $stmt->bindValue(':category_id', $this->category_id, PDO::PARAM_INT);

            return $stmt->execute();
        }
        return false;
    }

    public function updateCategory($data) {
        $this->category_title = $data['category_title'];

        $this->validate();               

        if(empty($this->errors)) {
            $sql = 'UPDATE categories SET category_title = :category_title WHERE category_id = :category_id';
            
            $db = static::getDB();
            $stmt = $db->prepare($sql);

            $stmt->bindValue(':category_title', $this->category_title, PDO::PARAM_STR);
            $stmt->bindValue(':category_id', $this->category_id, PDO::PARAM_INT);

            return $stmt->execute();
        }
        return false;
    }
}