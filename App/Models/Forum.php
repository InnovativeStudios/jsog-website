<?php

namespace App\Models;

use PDO;

class Forum extends \Core\Model {
    public $errors = [];

    public function __construct($data = []) {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        };
    }

    public function saveForumThreadPost() {
        $this->validateForumPost();

        if (empty($this->errors)) {

            $sql = 'INSERT INTO forum_posts (forum_post_author, forum_post_content, forum_post_thread_id, forum_post_date)
            VALUES (:forum_post_author, :forum_post_content, :forum_post_thread_id, NOW())';

            $db = static::getDB();
            $stmt = $db->prepare($sql);

            $stmt->bindValue(':forum_post_author', $this->forum_post_author, PDO::PARAM_STR);
            $stmt->bindValue(':forum_post_content', $this->forum_post_content, PDO::PARAM_STR);
            $stmt->bindValue(':forum_post_thread_id', $this->forum_post_thread_id, PDO::PARAM_STR);

            return $stmt->execute();
            $id = $db->lastInsertId();
        }
        return false;
    }

    public function saveForumThread() {
        $this->validateForumThread();

        if (empty($this->errors)) {

            $sql = 'INSERT INTO forum_threads (forum_thread_title, forum_thread_author, forum_thread_content, forum_thread_topic_id, forum_thread_date)
            VALUES (:forum_thread_title, :forum_thread_author, :forum_thread_content, :forum_thread_topic_id, NOW())';

            $db = static::getDB();
            $stmt = $db->prepare($sql);

            $stmt->bindValue(':forum_thread_title', $this->forum_thread_title, PDO::PARAM_STR);
            $stmt->bindValue(':forum_thread_author', $this->forum_thread_author, PDO::PARAM_STR);
            $stmt->bindValue(':forum_thread_content', $this->forum_thread_content, PDO::PARAM_STR);
            $stmt->bindValue(':forum_thread_topic_id', $this->forum_thread_topic_id, PDO::PARAM_STR);

            return $stmt->execute();
        }
        return false;
    }

    public function saveForumTopic() {
        $this->validateForumTopic();

        if (empty($this->errors)) {

            $sql = 'INSERT INTO forum_topics (forum_topic_title, forum_topic_description, forum_topic_category_id, forum_topic_thread_count)
            VALUES (:forum_topic_title, :forum_topic_description, :forum_topic_category_id, 1) 
            ON DUPLICATE KEY UPDATE forum_topic_thread_count = forum_topic_thread_count + 1';

            $db = static::getDB();
            $stmt = $db->prepare($sql);

            $stmt->bindValue(':forum_topic_title', $this->forum_topic_title, PDO::PARAM_STR);
            $stmt->bindValue(':forum_topic_description', $this->forum_topic_description, PDO::PARAM_STR);
            $stmt->bindValue(':forum_topic_category_id', $this->forum_topic_category_id, PDO::PARAM_STR);

            return $stmt->execute();
        }
        return false;
    }

    public function saveForumCategory() {

        $this->validateForumCategory();

        if (empty($this->errors)) {
            $sql = 'INSERT INTO forum_categories (forum_category_title)
            VALUES (:forum_category_title)';

            $db = static::getDB();
            $stmt = $db->prepare($sql);

            $stmt->bindValue(':forum_category_title', $this->forum_category_title, PDO::PARAM_STR);

            return $stmt->execute();
        }
        return false;
    }

    public function validateForumTopic() {
        if ($this->forum_topic_title == '') {
            $this->errors[] = 'Forum Topic Title is Required';
        }

        if ($this->forum_topic_description == '') {
            $this->errors[] = 'Forum Topic Description is Required';
        }

        if (static::forumTopicExists($this->forum_topic_title, $this->forum_topic_id ?? null)) {
            $this->errors[] = 'Forum Topic Already Exists';
        }
    }

    public function validateForumCategory() {
        if ($this->forum_category_title == '') {
            $this->errors[] = 'Forum Category Title is Required';
        }

        if (static::forumCategoryExists($this->forum_category_title, $this->forum_category_id ?? null)) {
            $this->errors[] = 'Forum Category Already Exists';
        }
    }

    public function validateForumThread() {

        if ($this->forum_thread_title == '') {
            $this->errors[] = 'Forum Thread Title is Required';
        }

        if ($this->forum_thread_content == '') {
            $this->errors[] = 'Forum Thread Content is Required';
        }

        if (static::forumThreadExists($this->forum_thread_title, $this->forum_thread_id ?? null)) {
            $this->errors[] = 'Forum Thread Already Exists';
        }
    }

    public function validateForumPost() {
        if ($this->forum_post_content == '') {
            $this->errors[] = 'Forum Post Content is Required';
        }
    }

    public static function forumTopicExists($forum_topic_title, $ignore_id = null) {
        $forum = static::findByTopicTitle($forum_topic_title);

        if ($forum) {
            if ($forum->forum_topic_id != $ignore_id) {
                return true;
            }
        }
        return false;
    }

    public static function forumThreadExists($forum_thread_title, $ignore_id = null) {
        $forum = static::findByThreadTitle($forum_thread_title);

        if ($forum) {
            if ($forum->forum_thread_id != $ignore_id) {
                return true;
            }
        }
        return false;
    }

    public static function forumCategoryExists($forum_category_title, $ignore_id = null) {
        $forum = static::findByCategoryTitle($forum_category_title);

        if ($forum) {
            if ($forum->forum_category_id != $ignore_id) {
                return true;
            }
        }
        return false;
    }

    public static function getAllForumCategories() {
        try {
            $db = static::getDB();
            $stmt = $db->query('SELECT forum_categories.*, forum_topics.* FROM forum_categories 
                                          LEFT JOIN forum_topics ON forum_categories.forum_category_id = forum_topics.forum_topic_category_id 
                                          ORDER BY forum_topic_id ASC');
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $results;
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function saveForumInfo() {
        if (empty($this->errors)) {
            $sql = 'INSERT INTO  forumInfo SELECT forum_topics.*, forum_threads.* FROM forum_topics 
            LEFT JOIN forum_threads ON forum_topics.forum_topic_id = forum_threads.forum_thread_topic_id';

            return $stmt->execute();
        }
        return false;
    }

    public static function getForumCategories() {
        try {
            $db = static::getDB();
            $stmt = $db->query('SELECT forum_category_id, forum_category_title FROM forum_categories ORDER BY forum_category_title DESC');
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $results;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function getAllForumTopics() {
        try {
            $db = static::getDB();
            $stmt = $db->query('SELECT * FROM forum_topics');
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $results;
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function getAllTopicThreads() {
        try {
            $db = static::getDB();
            $stmt = $db->query('SELECT forum_threads.*, forum_topics.* FROM forum_threads
                                          LEFT JOIN forum_topics ON forum_topics.forum_topic_id = forum_threads.forum_thread_topic_id
                                          GROUP BY forum_topics.forum_topic_id
                                          ORDER BY forum_thread_date DESC LIMIT 1');
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $results;
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function getAllThreadPosts() {
        try {
            $db = static::getDB();
            $stmt = $db->query('SELECT forum_threads.*, forum_posts.* FROM forum_threads 
                                          LEFT JOIN forum_posts ON forum_threads.forum_thread_id = forum_posts.forum_post_thread_id 
                                          WHERE forum_threads.forum_thread_id = forum_posts.forum_post_thread_id 
                                          ORDER BY forum_post_date DESC');
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $results;
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function findByCategoryTitle($forum_category_title) {
        $sql = 'SELECT * FROM forum_categories WHERE forum_category_title = :forum_category_title';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':forum_category_title', $forum_category_title, PDO::PARAM_STR);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }

    public static function findByTopicTitle($forum_topic_title) {
        $sql = 'SELECT * FROM forum_topics WHERE forum_topic_title = :forum_topic_title';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':forum_topic_title', $forum_topic_title, PDO::PARAM_STR);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }

    public static function findByThreadTitle($forum_thread_title) {
        $sql = 'SELECT * FROM forum_threads WHERE forum_thread_title = :forum_thread_title';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':forum_thread_title', $forum_thread_title, PDO::PARAM_STR);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }

    public static function findByCategoryID($forum_category_id) {
        $sql = 'SELECT * FROM forum_category WHERE forum_category_id = :forum_category_id';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':forum_category_id', $forum_category_id, PDO::PARAM_INT);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }

    public static function findByTopicID($forum_topic_id) {
        $sql = 'SELECT * FROM forum_topic WHERE forum_topic_id = :forum_topic_id';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':forum_topic_id', $forum_topic_id, PDO::PARAM_INT);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }

    public static function findByThreadID($forum_thread_id) {
        $sql = 'SELECT * FROM forum_thread WHERE forum_thread_id = :forum_thread_id';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':forum_thread_id', $forum_thread_id, PDO::PARAM_INT);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }

    public function getThreads() {
        $sql = 'SELECT forum_categories.*, forum_topics.*, forum_threads.* FROM forum_categories 
                LEFT JOIN forum_topics ON forum_categories.forum_category_id = forum_topics.forum_topic_category_id 
                LEFT JOIN forum_threads ON forum_threads.forum_thread_topic_id = forum_topics.forum_topic_id 
                WHERE forum_thread_topic_id = :topic_id';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':topic_id', $this->forum_topic_id, PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function getPosts() {
        $sql = 'SELECT forum_categories.*, forum_topics.*, forum_threads.*, forum_posts.* FROM forum_categories 
                LEFT JOIN forum_topics ON forum_categories.forum_category_id = forum_topics.forum_topic_category_id 
                LEFT JOIN forum_threads ON forum_threads.forum_thread_topic_id = forum_topics.forum_topic_id 
                LEFT JOIN forum_posts ON forum_posts.forum_post_thread_id = forum_threads.forum_thread_id
                WHERE forum_post_thread_id = :thread_id';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':thread_id', $this->forum_thread_id, PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public static function getTopicThreads() {
        $db = static::getDB();
        
        $stmt = $db->query('SELECT * FROM `forum_threads`
                            GROUP BY forum_thread_topic_id
                            ORDER BY forum_thread_date DESC');
     
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
     
        $data = [];
     
        foreach ($results as $result) {
     
            $data[$result['forum_thread_topic_id']] = $result;
     
        }
     
        return $data;
    }

    public static function getAllOnlineUsers() {
        try {
            $db = static::getDB();
            $stmt = $db->query('SELECT * FROM remembered_logins');
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $results;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function getUser() {
        if (isset($_SESSION['user_id'])) {
            return User::findByID($_SESSION['user_id']);
        } else {
            return static::loginFromRememberCookie();
        }
    }

    public static function getForumCategoryTitle($forum_category_title) {
        $sql = 'SELECT * FROM forum_categories WHERE forum_category_title = $forum_category_title';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue('forum_category_title', $forum_category_title, PDO::PARAM_INT);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }

    public function deleteForumCategory() {

        $this->validateForumCategory();               

        if(empty($this->errors)) {

            $sql = 'DELETE FROM forum_categories WHERE forum_category_id = :forum_category_id';
            
            $db = static::getDB();
            $stmt = $db->prepare($sql);
            $stmt->bindValue(':forum_category_id', $this->forum_category_id, PDO::PARAM_INT);

            return $stmt->execute();
        }
        return false;
    }

    public function updateForumCategory($data) {
        $this->forum_category_title = $data['forum_category_title'];

        $this->validate();               

        if(empty($this->errors)) {
            $sql = 'UPDATE forum_categories SET forum_category_title = :forum_category_title WHERE forum_category_id = :forum_category_id';
            
            $db = static::getDB();
            $stmt = $db->prepare($sql);

            $stmt->bindValue(':forum_category_title', $this->forum_category_title, PDO::PARAM_STR);
            $stmt->bindValue(':forum_category_id', $this->forum_category_id, PDO::PARAM_INT);

            return $stmt->execute();
        }
        return false;
    }

    public static function getForumTopicTitle($forum_topic_title) {
        $sql = 'SELECT * FROM forum_topics WHERE forum_topic_title = $forum_topic_title';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue('forum_topic_title', $forum_topic_title, PDO::PARAM_INT);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }

    public function deleteForumTopic() {

        $this->validateForumTopic();               

        if(empty($this->errors)) {

            $sql = 'DELETE FROM forum_topics WHERE forum_topic_id = :forum_topic_id';
            
            $db = static::getDB();
            $stmt = $db->prepare($sql);
            $stmt->bindValue(':forum_topic_id', $this->forum_topic_id, PDO::PARAM_INT);

            return $stmt->execute();
        }
        return false;
    }

    public function updateForumTopic($data) {
        $this->forum_topic_title = $data['forum_topic_title'];
        $this->forum_topic_description = $data['forum_topic_description'];

        $this->validate();               

        if(empty($this->errors)) {
            $sql = 'UPDATE forum_topics SET forum_topic_title = :forum_topic_title, forum_topic_description = :forum_topic_description WHERE forum_topic_id = :forum_topic_id';
            
            $db = static::getDB();
            $stmt = $db->prepare($sql);

            $stmt->bindValue(':forum_topic_title', $this->forum_topic_title, PDO::PARAM_STR);
            $stmt->bindValue(':forum_topic_description', $this->forum_topic_description, PDO::PARAM_STR);
            $stmt->bindValue(':forum_topic_id', $this->forum_topic_id, PDO::PARAM_INT);

            return $stmt->execute();
        }
        return false;
    }
}