<?php
/**
 * Created by PhpStorm.
 * User: Jacob Schmidt
 * Date: 8/14/2017
 * Time: 10:30 PM
 */

namespace App\Models;

use PDO;


class UserGroup extends \Core\Model
{
    public $errors = [];

    public function __construct($data = [])
    {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        };
    }

    public function save() {

        $this->validate();

        if (empty($this->errors)) {
            $sql = 'INSERT INTO user_groups (user_group_title)
            VALUES (:user_group_title)';

            $db = static::getDB();
            $stmt = $db->prepare($sql);

            $stmt->bindValue(':user_group_title', $this->user_group_title, PDO::PARAM_STR);

            return $stmt->execute();
        }
        return false;
    }

    public function validate() {
        if ($this->user_group_title == '') {
            $this->errors[] = 'User Group Title is Required';
        }
    }

    public static function UserGroupExists($user_group_title, $ignore_id = null) {
        $user_group = static::findByUserGroupTitle($user_group_title);

        if ($user_group) {
            if ($user_group->user_group_id != $ignore_id) {
                return true;
            }
        }
        return false;
    }

    public static function getUserGroupTitle($user_group_title) {
        $sql = 'SELECT * FROM user_groups WHERE user_group_title = $user_group_title';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue('user_group_title', $user_group_title, PDO::PARAM_INT);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }

    public static function findByUserGroupTitle($user_group_title) {
        $sql = 'SELECT * FROM user_groups WHERE user_group_title = :user_group_title';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':user_group_title', $user_group_title, PDO::PARAM_STR);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }

    public static function findByUserGroupID($user_group_id) {
        $sql = 'SELECT * FROM user_groups WHERE user_group_id = :user_group_id';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':user_group_id', $user_group_id, PDO::PARAM_INT);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }

    public function deleteUserGroup() {

        $this->validate();               
    
        if(empty($this->errors)) {             
    
            $sql = 'DELETE FROM user_groups WHERE user_group_id = :user_group_id';
            
            $db = static::getDB();
            $stmt = $db->prepare($sql);
            $stmt->bindValue(':user_group_id', $this->user_group_id, PDO::PARAM_INT);
    
            return $stmt->execute();
        }
        return false;
    }

    public function updateUserGroup($data) {
        $this->user_group_title = $data['user_group_title'];

        $this->validate();               

        if(empty($this->errors)) {
            $sql = 'UPDATE user_groups SET user_group_title = :user_group_title WHERE user_group_id = :user_group_id';
            
            $db = static::getDB();
            $stmt = $db->prepare($sql);

            $stmt->bindValue(':category_title', $this->category_title, PDO::PARAM_STR);
            $stmt->bindValue(':category_id', $this->category_id, PDO::PARAM_INT);

            return $stmt->execute();
        }
        return false;
    }
}